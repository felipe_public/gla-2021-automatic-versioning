# Changelog
All notable changes to this project will be documented in this file.

See [standard-version](https://github.com/conventional-changelog/standard-version) for commit
guidelines. This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
### [2.0.1](https://gitlab.com/felipe_public/gla-2021-automatic-versioning/compare/v2.0.0...v2.0.1) (2021-11-14)


### CI

* adds version preview job ([abff8c5](https://gitlab.com/felipe_public/gla-2021-automatic-versioning/commit/abff8c58ba5e993bb252ed911d22f4edd09e65db))

## [2.0.0](https://gitlab.com/felipe_public/gla-2021-automatic-versioning/compare/v1.1.0...v2.0.0) (2021-11-14)


### ⚠ BREAKING CHANGES

* this new API is incompatible with
the previous and therefore you should be careful when
updating.

### Features

* changes API ([e1bf3cd](https://gitlab.com/felipe_public/gla-2021-automatic-versioning/commit/e1bf3cd1e9da4206e7aac186a8f2b4f73fa95a3c))


### Docs

* adds README file ([de03aed](https://gitlab.com/felipe_public/gla-2021-automatic-versioning/commit/de03aed358e4b1af1b3884f9a779663f9b6235c4))

## [1.1.0](https://gitlab.com/felipe_public/gla-2021-automatic-versioning/compare/v1.0.0...v1.1.0) (2021-11-14)


### Features

* adds license file ([9fbf908](https://gitlab.com/felipe_public/gla-2021-automatic-versioning/commit/9fbf908c8d3046ac7391a263c8e1b2920d0dab02))


### CI

* adds automatic versioning with CI ([19397f8](https://gitlab.com/felipe_public/gla-2021-automatic-versioning/commit/19397f8378f2f5e846d319c155bf75e14b19d9c6))
* fix on the missing push command ([ad07d2a](https://gitlab.com/felipe_public/gla-2021-automatic-versioning/commit/ad07d2aec30bf324a2257160a34c5b7c04768c90))
* set the main as variable ([df2f40b](https://gitlab.com/felipe_public/gla-2021-automatic-versioning/commit/df2f40b4e5e55277a64c4e4aae548b2cf441ae45))

## 1.0.0 (2021-11-14)


### Features

* initial version with folder structure ([eb2111b](https://gitlab.com/felipe_public/gla-2021-automatic-versioning/commit/eb2111bc54919fe31a705ae45b0dd088877b124c))
